def swapElements(arr:Array[Int]) = {
  (for {index <- 0 until arr.length}
  yield if(index == arr.length -1 & arr.length % 2 == 1) arr(index) else if(index % 2 == 0) arr(index + 1) else arr(index-1)).toArray
}

def swapElements2(arr:Array[Int]) = {
  for (elem <- arr.grouped(2);
       single <- elem.reverse
  ) yield single
}

print(swapElements(Array(2, 1, 4, 3)).toList)
print(swapElements(Array(2, 1, 4, 3,5)).toList)
print(swapElements2(Array(2, 1, 4, 3,5)).toList)




def timezones()= {
  for( element <- java.util.TimeZone.getAvailableIDs if element.startsWith("America")) yield element.split("/")(1)
}

println("Sorted: " + timezones().toList.sorted)




def removeFirstNeg(a: Array[Int]):Array[Int] = {
  val indexes = (for(index <- 0 until a.length; if a(index) < 0 ) yield index).drop(1)
  println("Indexy: " + indexes)
  val b = a.toBuffer
  indexes.foreach(b.remove(_))
  b.toArray
}

print(Array(2,-1,5,0,-5,10).toList)
print(removeFirstNeg(Array(2,-1,5,0,-5,10)).toList)




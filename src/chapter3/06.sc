def revSorted(a: Array[Int]) = {
  a.sortWith(_>_)
}

print(revSorted(Array(2,-1,5,0,-5,10)).toList)

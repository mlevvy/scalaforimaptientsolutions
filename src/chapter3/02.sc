val input = Array(2, 1, 4, 3, 5)

for(i <- 0 until (if(input.length % 2 == 0) input.length else input.length -1,2)){
   print(i)
   val tmp = input(i)
   input(i) = input(i+1)
   input(i+1) = tmp
}

print(input.toList)

def someFun(a: Array[Int]) = {
  val pos = for(elem <- a; if elem >= 0) yield elem;
  val neg = for{elem <- a if elem < 0} yield elem;
  Array.concat(pos,neg).toList
}

print(someFun(Array(2,-1,5,0,-5,10)))
import java.awt.datatransfer._
import collection.JavaConversions._
val flavors = SystemFlavorMap.getDefaultFlavorMap().asInstanceOf[SystemFlavorMap]

val nativeFlavours = asScalaBuffer(flavors.getNativesForFlavor(DataFlavor.imageFlavor))

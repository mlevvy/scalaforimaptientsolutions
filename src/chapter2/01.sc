def signum(n : Int) = {
  if(n > 0) 1
  else if (n < 0) -1
  else 0
}


print(signum(20))
print(signum(-20))
print(signum(0))